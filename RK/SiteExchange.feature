# language: ru

Функционал: Загрузка реализации с сайта

Хочу проверить код загрузки xml файла с данными с сайта в 1с. Он должен создать реализацию 
- в одном случае просто заполнить Товары
- в втором случае заполнить таблицу Детали 

# Контекст сценария выполняется всегда перед каждым сценарием

Контекст:
  Дано узел обмена с сайтом который предназначен для загрузки заказов (береться с константы) "Вузол обміну замовленнями" 
  И Я захожу в 1с под ролью "ПолныеПрава" 
# Каждый сценарий состоит из последовательных связанных шагов

Сценарий: загрузки заказов
  Дано макет с рание подготовленными xml данными из сайта в файле "СценарийЗагрузкиССайтаПростой", сразу очищаю номер "164993" в прведведущем тестовом документе     
  Когда я открою узел я изменю значения "Способ обмена данными" с 'Выгружать на сайте' на "Выгружать в каталоге на диске" 
  И установлю значение поля "Файл загрузки заказов" временным значением файла где будет храниться мой заготовленый файл "Order.xml"
  И я нажму на кнопку "Выполнить обмен" 
  То у меня должен был создаться документ и я сравниваю его с макетом "МакеПравельноСозданойРеализациПростой"
    

Сценарий: возврат настройек к предведущему состоянию
  Когда Я открываю узел обмена из контекста И возвращаю значения в предведущие "Способ обмена данными" , а также "Файл загрузки заказов" 

Сценарий: Загрузка заказов ЗаказПлиты
  Дано макет с рание подготовленными xml данными из сайта в файле "СценарийЗагрузкиССайтаДетали", сразу очищаю номер "200652" в прведведущем тестовом документе
  Когда я открою узел я изменю значения "Способ обмена данными" с 'Выгружать на сайте' на "Выгружать в каталоге на диске" 
  И установлю значение поля "Файл загрузки заказов" временным значением файла где будет храниться мой заготовленый файл "Order.xml" 
  И я нажму на кнопку "Выполнить обмен" 
  То у меня должен был создаться документ и я сравниваю его с макетом "МакеПравельноСозданойРеализациДетали"

Сценарий: возврат настройек к предведущему состоянию
  Когда Я открываю узел обмена из контекста И возвращаю значения в предведущие "Способ обмена данными" , а также "Файл загрузки заказов"   