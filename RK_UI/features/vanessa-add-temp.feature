﻿#language: ru

@IgnoreOnCIMainBuild

Функционал: <описание фичи>

Как <Роль>
Я хочу <описание функционала>
Чтобы <бизнес-эффект>

Контекст:
	Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий


Сценарий: <описание сценария>

	Когда Я нажимаю кнопку командного интерфейса 'Номенклатура плит'
	Тогда открылось окно 'Номенклатура плит'
	И в таблице "СписокРодителей" я перехожу к строке:
		| Ссылка |
		| ПВХ    |
	И в таблице "Список" я перехожу к строке:
		| Виробник | Декор | Дод.од.вим. | Картинка заповнена | Найменування | Од.вим. |
		| MAAG     | 26/1  | п.м.        | Нет                | 'Акація '    | п.м.    |
	И в таблице "Список" я перехожу к строке:
		| Виробник | Декор   | Дод.од.вим. | Картинка заповнена | Найменування | Од.вим. |
		| Кромаг   | '30.01' | п.м.        | Да                 | Акація       | п.м.    |
	И в таблице "СписокХарактеристик" я перехожу к строке:
		| кв.м.    | Код         | Кусок | Наименование              | Товщина | Характеристика власник   | Ширина  | шт.    |
		| '7,3500' | '000299414' | Нет   | 2 Акація 30.01  Кромаг 22 | 2       | 2 Акація 30.01 Кромаг 22 | '22,00' | '7,35' |
	И в таблице "СписокХарактеристик" я перехожу к строке:
		| кв.м.    | Код         | Кусок | Наименование              | Товщина | Ширина  | шт.    |
		| '3,9500' | '000001036' | Нет   | 1 Акація 30.01  Кромаг 22 | 1       | '22,00' | '3,95' |
	И в таблице "СписокХарактеристик" я перехожу к строке:
		| кв.м.    | Код         | Кусок | Наименование              | Товщина | Ширина  | шт.    |
		| '3,9500' | '000001036' | Нет   | 1 Акація 30.01  Кромаг 22 | 1       | '22,00' | '3,95' |
	И в таблице "СписокХарактеристик" я выбираю текущую строку
	Если элемент "Длинна" присутствует на форме Тогда 
	Тогда из выпадающего списка с именем "Длинна" я выбираю по строке "ДлиннаЧисло" 

