#language: ru

@ExportScenarios
@IgnoreOnCIMainBuild
@Tree

Функционал: Заполнение документ прайс

Сценарий: Заполняю настройки компоновщика прайс для Группы "Группа" и Номерклатуры "НоменклатураНаименование"

	Тогда открылось окно 'Прайс (создание)'
	И я изменяю флаг 'Довільні налаштування'
	И я разворачиваю группу "Настройки"
	И я изменю настройки Прайса
		# Пока убираю настройку по "Заполнение прайса"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я перехожу к строке:
			| Вид сравнения | Использование | Поле                                                                       | Применение | Режим отображения |
			| В группе      | Да            | ХарактеристикаНоменклатуры.Заповнення прайса (Характеристики номенклатури) | Обычное    | Быстрый доступ    |
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я изменяю флаг 'Использование'
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я завершаю редактирование строки
		# Устанавливаю родителя "ЛДСП"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я перехожу к строке:
			| Вид сравнения | Использование | Поле     | Применение | Режим отображения |
			| В группе      | Да            | Родитель | Обычное    | Быстрый доступ    |
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я активизирую поле "Значение"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я выбираю текущую строку
		# И в таблице "КомпоновщикНастроекНастройкиОтбор" из выпадающего списка "Значение" я выбираю точное значение "Группа"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" из выпадающего списка "Значение" я выбираю по строке "Группа"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я завершаю редактирование строки
		# Убираю из отбора "Толщина" - так как буду заполнять по всех толщинах
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я перехожу к строке:
			| Вид сравнения | Использование | Поле    | Применение | Режим отображения |
			| Равно         | Да            | Товщина | Обычное    | Быстрый доступ    |
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я активизирую поле "Использование"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я изменяю флаг 'Использование'
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я завершаю редактирование строки
		# Добавляю новый отбор по номенклатуре
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я нажимаю на кнопку с именем 'КомпоновщикНастроекНастройкиОтборДобавитьЭлементОтбора'
		И в таблице "КомпоновщикНастроекНастройкиОтбор" из выпадающего списка "Поле" я выбираю точное значение 'Номенклатура'
		И я перехожу к следующему реквизиту
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я нажимаю кнопку выбора у реквизита "Вид сравнения"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я активизирую поле "Значение"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" из выпадающего списка "Значение" я выбираю по строке  "НоменклатураНаименование"
		И в таблице "КомпоновщикНастроекНастройкиОтбор" я завершаю редактирование строки

	И в таблице "ЦенаНоменклатуры" я нажимаю на кнопку с именем 'ЦенаНоменклатурыЗаполнить'
	И Пауза 1

Сценарий: Заполняю в таблицу Прайса "ЦенаПрихода1" "ЦенаПрихода2" "ХарактеристикаНоменклатуры1" "ХарактеристикаНоменклатуры2", цену расхода "ЦенаРасхода1" "ЦенаРасхода2" 

	И после заполнение таблицы я устанавливаю расходные цены
		И в таблице "ЦенаНоменклатуры" я перехожу к строке:
			| N | кв м.          | Найменування                  |
			| 2 | "ЦенаПрихода2" | "ХарактеристикаНоменклатуры2" |
		И в таблице "ЦенаНоменклатуры" я активизирую поле с именем "ЦенаНоменклатурыРозходнаяЦенаМКв"
		И в таблице "ЦенаНоменклатуры" я активизирую поле с именем "ЦенаНоменклатурыРозходнаяЦена"
		И в таблице "ЦенаНоменклатуры" в поле с именем 'ЦенаНоменклатурыРозходнаяЦена' я ввожу текст "ЦенаРасхода2"
		И в таблице "ЦенаНоменклатуры" я завершаю редактирование строки
		И в таблице "ЦенаНоменклатуры" я перехожу к строке:
			| N | кв м.          | Найменування                  |
			| 1 | "ЦенаПрихода1" | "ХарактеристикаНоменклатуры1" |
		И в таблице "ЦенаНоменклатуры" в поле с именем 'ЦенаНоменклатурыРозходнаяЦена' я ввожу текст "ЦенаРасхода1"
		И в таблице "ЦенаНоменклатуры" я завершаю редактирование строки