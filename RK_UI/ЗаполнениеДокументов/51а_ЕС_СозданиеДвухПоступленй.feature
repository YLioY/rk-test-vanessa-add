#language: ru

@Tree
@ExportScenarios
@IgnoreOnCIMainBuild

Функционал: Я создаю и заполняю документ поступление товаров

Как 	<ПолныеПрава>	
Я хочу 	<создать поступление товаров , для новой номенклатуры и внести этим документом приходные цены провести его>
Чтобы 	<небыло ошибок при работе с поступлениями , в будущем можно было проверить расход по партиях>  

Контекст: 
	Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий


Сценарий: Создаю два документа Поступления товаров 
 
	# Поиск в интерфейсе и открытие документа
	Когда Я нажимаю кнопку командного интерфейса 'Поступлення товарів'
	Тогда открылось окно 'Поступлення товарів'
	# Создаю первый документ
	Тогда Я хочу создать первый документ поступления
		И я нажимаю на кнопку с именем 'ФормаСоздать'
		Тогда открылось окно 'Поступлення товарів (создание)'

		И Заполнить документ Поступление товаров

		И я нажимаю на кнопку 'Провести'
		И я запоминаю значение поля "Номер" как "НомерДокументаПоступления1"
		И я запоминаю значение поля с именем "Дата" как "ДатаДокументаПоступление1"
		И Я запоминаю значение выражения 'Формат(Дата(Контекст.ДатаДокументаПоступление1),"ДФ=dd.MM.yyyy")' в переменную "ДатаДокументаПоступление1"


	И Я закрываю окно 'Поступлення товарів * от *'
	Тогда открылось окно 'Поступлення товарів'
	
	Тогда Я хочу создать второй документ поступления
		# Создаю второй документ
		И я нажимаю на кнопку с именем 'ФормаСоздать'
		Тогда открылось окно 'Поступлення товарів (создание)'

		И Заполнить документ Поступление товаров

		И я нажимаю на кнопку 'Провести'
		И я запоминаю значение поля "Номер" как "НомерДокументаПоступления2"
		И я запоминаю значение поля с именем "Дата" как "ДатаДокументаПоступление2"
		И Я запоминаю значение выражения 'Формат(Дата(Контекст.ДатаДокументаПоступление2),"ДФ=dd.MM.yyyy")' в переменную "ДатаДокументаПоступление2"


Сценарий: Я хочу найти список проводок документа Поступления по регистру Товары на складах и сверить данные, для только что созданых поступлений
	
	Дано Я открываю основную форму регистра накопления "ТоварыНаСкладах"
	И в таблице "Список" я активизирую поле "Период"
	И в таблице "Список" я активизирую поле с именем "Регистратор"

	И Нахожу регистратор по номеру "$НомерДокументаПоступления1$" и по Дате "$ДатаДокументаПоступление1$"

	И Я запоминаю значение выражения 'Контекст.ДокументПоступление' в переменную "ДокументПоступление1"

	И Нахожу регистратор по номеру "$НомерДокументаПоступления2$" и по Дате "$ДатаДокументаПоступление2$"

	И Я запоминаю значение выражения 'Контекст.ДокументПоступление' в переменную "ДокументПоступление2"
	

	 
	

